﻿namespace FortCodeExercises.Exercise1.Interfaces
{
    public interface IMachine
    {
        public string Name { get; }
        public string Description { get; }
        public MachineTypes Type { get; }
        public Colors BaseColor { get; }
        public Colors TrimColor { get; }
        public bool HasMaxSpeed { get; }

        public int GetMaxSpeed(bool noMax = false);

    }
}

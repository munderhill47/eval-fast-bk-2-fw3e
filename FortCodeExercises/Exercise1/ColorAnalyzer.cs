﻿using System.Collections.Generic;

namespace FortCodeExercises.Exercise1
{
    public static class ColorAnalyzer
    {
        private static IList<Colors> _darkColors = new List<Colors>() { Colors.Red, Colors.Green, Colors.Black, Colors.Crimson };

        public static bool IsDark(Colors color)
        {
            return _darkColors.Contains(color);
        }
    }
}

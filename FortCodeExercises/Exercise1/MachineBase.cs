﻿using FortCodeExercises.Exercise1.Interfaces;

namespace FortCodeExercises.Exercise1
{
    public abstract class MachineBase : IMachine
    {
        public string Name { protected set; get; }
        public string Description { protected set; get; } = string.Empty;
        public MachineTypes Type { protected set; get; } = MachineTypes.NotValidMachineType;
        public Colors BaseColor { protected set; get; } = BaseColors.White;
        public Colors TrimColor { protected set; get; } = TrimColors.None;
        public bool HasMaxSpeed { protected set; get; } = true;
        
        public virtual int GetMaxSpeed(bool noMax = false) => DefaultMaxSpeed;

        protected const int DefaultMaxSpeed = 70;

        protected void SetDependentProperties()
        {
            Name = Type.ToString().ToLower();
            Description = $" {BaseColor.ToString().ToLower()} {Name} [{GetMaxSpeed(HasMaxSpeed)}].";

            SetTrimColor();
        }

        protected virtual void SetTrimColor()
        {
            // Don't set anything by default
        }
    }
}

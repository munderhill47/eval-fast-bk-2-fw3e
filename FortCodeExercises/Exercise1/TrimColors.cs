﻿namespace FortCodeExercises.Exercise1
{
    public static class TrimColors
    {
        public static Colors None => Colors.None;
        public static Colors Black => Colors.Black;
        public static Colors White => Colors.White;
        public static Colors Gold => Colors.Gold;
        public static Colors Silver => Colors.Silver;
    }
}

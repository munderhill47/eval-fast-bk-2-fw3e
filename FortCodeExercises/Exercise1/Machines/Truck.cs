﻿namespace FortCodeExercises.Exercise1.Machines
{
    public class Truck : MachineBase
    {
        public Truck()
        {
            Type = MachineTypes.Truck;
            BaseColor = BaseColors.Yellow;

            SetDependentProperties();
        }

        protected override void SetTrimColor()
        {
            TrimColor = ColorAnalyzer.IsDark(BaseColor) ? TrimColors.Silver : TrimColors.None;
        }
    }
}

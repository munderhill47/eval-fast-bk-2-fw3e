﻿namespace FortCodeExercises.Exercise1.Machines
{
    public class Car : MachineBase
    {
        public Car()
        {
            Type = MachineTypes.Car;
            BaseColor = BaseColors.Brown;
            HasMaxSpeed = false;

            SetDependentProperties();
        }

        public override int GetMaxSpeed(bool noMax = false)
        {
            return noMax ? 90 : DefaultMaxSpeed;
        }
    }
}

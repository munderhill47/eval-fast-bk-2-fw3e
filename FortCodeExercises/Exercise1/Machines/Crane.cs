﻿namespace FortCodeExercises.Exercise1.Machines
{
    public class Crane : MachineBase
    {
        public Crane()
        {
            Type = MachineTypes.Crane;
            BaseColor = BaseColors.Blue;

            SetDependentProperties();
        }

        public override int GetMaxSpeed(bool noMax = false)
        {
            return noMax ? 75 : DefaultMaxSpeed;
        }

        protected override void SetTrimColor()
        {
            TrimColor = ColorAnalyzer.IsDark(BaseColor) ? TrimColors.Black : TrimColors.White; 
        }
    }
}

﻿namespace FortCodeExercises.Exercise1.Machines
{
    public class Bulldozer : MachineBase
    {
        public Bulldozer()
        {
            Type = MachineTypes.Bulldozer;
            BaseColor = BaseColors.Red;

            SetDependentProperties();
        }

        public override int GetMaxSpeed(bool noMax = false)
        {
            return noMax ? 80 : DefaultMaxSpeed;
        }
    }
}

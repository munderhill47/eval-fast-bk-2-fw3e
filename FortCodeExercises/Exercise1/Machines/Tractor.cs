﻿namespace FortCodeExercises.Exercise1.Machines
{
    internal class Tractor : MachineBase
    {
        public Tractor()
        {
            Type = MachineTypes.Tractor;
            BaseColor = BaseColors.Green;

            SetDependentProperties();
        }

        public override int GetMaxSpeed(bool noMax = false)
        {
            return noMax ? 90 : 60;
        }

        protected override void SetTrimColor()
        {
            TrimColor = ColorAnalyzer.IsDark(BaseColor) ? TrimColors.Gold : TrimColors.None;
        }
    }
}

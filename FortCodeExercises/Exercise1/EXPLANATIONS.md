﻿# FORT Technical Exercise Explanation

## Observations on Refactoring
Since the task was to refactor the class, the first step was to create some tests to ensure that the refactored class would behave the same way as the original class. These tests are in the *FortCodeTests* project.
I did take a few liberties though, specifically in changing the case of the properties to be PascalCase (my preference), but should be defined in the coding standards for the company. If the existing class was used in an application I would keep them the same. 

The original class had a lot of if/else conditionals that could have been refactored into a switch statement, but a class should only have a single responsibility so I chose to refactor into an abstract base class (***MachineBase.cs***) and separate concrete classes for each of the machine type. Any special behavior for a specific machine type was implemented in that concrete class.

To abstract the creation of a specific machine I added a factory class (***MachineFactory.cs***) that hides any complexity of machine creation and is extensible by simply adding another entry to the constructor dictionary.

```
        private static IDictionary<MachineTypes, Func<IMachine>> _machineConstructors = new Dictionary<MachineTypes, Func<IMachine>>()
        { 
            { MachineTypes.Bulldozer, () => new Bulldozer() },
            { MachineTypes.Car, () => new Car() },
            { MachineTypes.Crane, () => new Crane() },
            { MachineTypes.Tractor, () => new Tractor() },
            { MachineTypes.Truck, () => new Truck() },
        };
```

I also refactored the machine types and colors into enumerations because using magic numbers or strings makes the code less readable and maintainable. Strings present the problem that they will only be evaluated at runtime and prone to typing errors.
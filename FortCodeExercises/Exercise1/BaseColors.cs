﻿namespace FortCodeExercises.Exercise1
{
    public static class BaseColors
    {
        public static Colors White => Colors.White;
        public static Colors Red => Colors.Red;
        public static Colors Blue => Colors.Blue;
        public static Colors Green => Colors.Green;
        public static Colors Yellow => Colors.Yellow;
        public static Colors Brown => Colors.Brown;
    }
}

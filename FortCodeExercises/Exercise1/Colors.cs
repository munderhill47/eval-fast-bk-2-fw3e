﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public enum Colors
    {
        None,
        Red,
        Blue,
        Green,
        Yellow,
        Brown,
        Black,
        White,
        Gold,
        Silver,
        Beige,
        BabyBlue,
        Crimson
    }
}

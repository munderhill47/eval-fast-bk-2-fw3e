﻿using FortCodeExercises.Exercise1.Interfaces;
using FortCodeExercises.Exercise1.Machines;
using System;
using System.Collections.Generic;

namespace FortCodeExercises.Exercise1
{
    public static class MachineFactory
    {
        private static IDictionary<MachineTypes, Func<IMachine>> _machineConstructors = new Dictionary<MachineTypes, Func<IMachine>>()
        { 
            { MachineTypes.Bulldozer, () => new Bulldozer() },
            { MachineTypes.Car, () => new Car() },
            { MachineTypes.Crane, () => new Crane() },
            { MachineTypes.Tractor, () => new Tractor() },
            { MachineTypes.Truck, () => new Truck() },
        };

        public static IMachine Create(MachineTypes machineType)
        {
            if (!_machineConstructors.ContainsKey(machineType))
            {
                throw new ArgumentException($"Missing constructor entry for {machineType}.");
            }

            return _machineConstructors[machineType]();
        }
    }
}

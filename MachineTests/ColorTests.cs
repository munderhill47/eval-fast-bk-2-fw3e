﻿using FortCodeExercises.Exercise1;
using Xunit;

namespace FortCodeTests
{
    public class ColorTests
    {
        /// <summary>
        /// Tests for Original Class
        /// </summary>

        [Fact]
        public void RedShouldBeDark()
        {
            var machine = new Machine();

            Assert.True(machine.isDark("red"));
        }


        [Fact]
        public void CrimsonShouldBeDark()
        {
            var machine = new Machine();

            Assert.True(machine.isDark("crimson"));
        }

        [Fact]
        public void BlackShouldBeDark()
        {
            var machine = new Machine();

            Assert.True(machine.isDark("black"));
        }

        [Fact]
        public void GreenShouldBeDark()
        {
            var machine = new Machine();

            Assert.True(machine.isDark("green"));
        }

        [Fact]
        public void YellowShouldNotBeDark()
        {
            var machine = new Machine();

            Assert.False(machine.isDark("yellow"));
        }

        [Fact]
        public void WhiteShouldNotBeDark()
        {
            var machine = new Machine();

            Assert.False(machine.isDark("white"));
        }

        [Fact]
        public void BeigeShouldNotBeDark()
        {
            var machine = new Machine();

            Assert.False(machine.isDark("beige"));
        }

        [Fact]
        public void BabyBlueShouldNotBeDark()
        {
            var machine = new Machine();

            Assert.False(machine.isDark("babyblue"));
        }

        /// <summary>
        /// Tests for Refactored Class
        /// </summary>


    }
}

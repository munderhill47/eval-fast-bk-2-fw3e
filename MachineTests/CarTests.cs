﻿using FortCodeExercises.Exercise1;
using Xunit;


namespace FortCodeTests
{
    public class CarTests
    {
        /// <summary>
        /// Tests for Original Class
        /// </summary>

        [Fact]
        public void NameShouldBeCar()
        {
            var machine = new Machine() { type = 4 };

            Assert.Equal("car", machine.name);
        }

        [Fact]
        public void DescriptionShouldBeBrownCar70()
        {
            var machine = new Machine() { type = 4 };

            Assert.Equal(" brown car [70].", machine.description);
        }

        [Fact]
        public void ColorShouldBeBrownForCar()
        {
            var machine = new Machine() { type = 4 };

            Assert.Equal("brown", machine.color);
        }

        [Fact]
        public void TrimColorShouldBeEmptyForCar()
        {
            var machine = new Machine() { type = 4 };

            Assert.Equal(string.Empty, machine.trimColor);
        }

        [Fact]
        public void MaxSpeedShouldBe90ForCarWithNoMax()
        {
            var machine = new Machine() { type = 4 };

            Assert.Equal(90, machine.getMaxSpeed(machine.type, true));
        }


        [Fact]
        public void MaxSpeedShouldBe70ForCarWithMax()
        {
            var machine = new Machine() { type = 4 };

            Assert.Equal(70, machine.getMaxSpeed(machine.type));
        }

        /// <summary>
        /// Tests for Refactored Class
        /// </summary>

        [Fact]
        public void NameShouldBeCar_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Car);

            Assert.Equal("car", machine.Name);
        }

        [Fact]
        public void DescriptionShouldBeBrownCar70_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Car);

            Assert.Equal(" brown car [70].", machine.Description);
        }

        [Fact]
        public void ColorShouldBeBrownForCar_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Car);

            Assert.Equal(BaseColors.Brown, machine.BaseColor);
        }

        [Fact]
        public void TrimColorShouldBeEmptyForCar_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Car);

            Assert.Equal(TrimColors.None, machine.TrimColor);
        }

        [Fact]
        public void MaxSpeedShouldBe90ForCarWithNoMax_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Car);

            Assert.Equal(90, machine.GetMaxSpeed(true));
        }


        [Fact]
        public void MaxSpeedShouldBe70ForCarWithMax_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Car);

            Assert.Equal(70, machine.GetMaxSpeed());
        }
    }
}

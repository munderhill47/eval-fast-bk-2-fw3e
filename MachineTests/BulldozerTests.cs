using FortCodeExercises.Exercise1;
using Xunit;

namespace FortCodeTests
{
    public class BulldozerTests
    {
        /// <summary>
        /// Tests for Original Class
        /// </summary>

        [Fact]
        public void NameShouldBeBulldozer()
        {
            var machine = new Machine();

            Assert.Equal("bulldozer", machine.name);
        }

        [Fact]
        public void DescriptionShouldBeRedBulldozer80()
        {
            var machine = new Machine();

            Assert.Equal(" red bulldozer [80].", machine.description);
        }

        [Fact]
        public void ColorShouldBeRedForBulldozer()
        {
            var machine = new Machine();

            Assert.Equal("red", machine.color);
        }

        [Fact]
        public void TrimColorShouldBeRedForBulldozer()
        {
            var machine = new Machine();

            Assert.Equal(string.Empty, machine.trimColor);
        }

        [Fact]
        public void MaxSpeedShouldBe80ForBulldozerWithNoMax()
        {
            var machine = new Machine() { type = 0 };

            Assert.Equal(80, machine.getMaxSpeed(machine.type, true));
        }


        [Fact]
        public void MaxSpeedShouldBe70ForBulldozerWithMax()
        {
            var machine = new Machine() { type = 0 };

            Assert.Equal(70, machine.getMaxSpeed(machine.type));
        }

        /// <summary>
        /// Tests for Refactored Class
        /// </summary>

        [Fact]
        public void NameShouldBeBulldozer_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Bulldozer);

            Assert.Equal("bulldozer", machine.Name);
        }

        [Fact]
        public void DescriptionShouldBeRedBulldozer80_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Bulldozer);

            Assert.Equal(" red bulldozer [80].", machine.Description);
        }

        [Fact]
        public void ColorShouldBeRedForBulldozer_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Bulldozer);

            Assert.Equal(BaseColors.Red, machine.BaseColor);
        }

        [Fact]
        public void TrimColorShouldBeRedForBulldozer_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Bulldozer);

            Assert.Equal(TrimColors.None, machine.TrimColor);
        }

        [Fact]
        public void MaxSpeedShouldBe80ForBulldozerWithNoMax_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Bulldozer);

            Assert.Equal(80, machine.GetMaxSpeed(true));
        }


        [Fact]
        public void MaxSpeedShouldBe70ForBulldozerWithMax_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Bulldozer);

            Assert.Equal(70, machine.GetMaxSpeed());
        }
    }
}
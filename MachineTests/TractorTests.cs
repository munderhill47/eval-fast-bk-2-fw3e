﻿using FortCodeExercises.Exercise1;
using Xunit;


namespace FortCodeTests
{
    public class TractorTests
    {
        /// <summary>
        /// Tests for Original Class
        /// </summary>

        [Fact]
        public void NameShouldBeTractor()
        {
            var machine = new Machine() { type = 2 };

            Assert.Equal("tractor", machine.name);
        }

        [Fact]
        public void DescriptionShouldBeGreenTractorr90()
        {
            var machine = new Machine() {  type = 2 };

            Assert.Equal(" green tractor [90].", machine.description);
        }

        [Fact]
        public void ColorShouldBeGreenForTractor()
        {
            var machine = new Machine() { type = 2 };

            Assert.Equal("green", machine.color);
        }

        [Fact]
        public void TrimColorShouldBeGoldForTractor()
        {
            var machine = new Machine() { type = 2 };

            Assert.Equal("gold", machine.trimColor);
        }

        [Fact]
        public void MaxSpeedShouldBe90ForTractorWithNoMax()
        {
            var machine = new Machine() { type = 2 };

            Assert.Equal(90, machine.getMaxSpeed(machine.type, true));
        }


        [Fact]
        public void MaxSpeedShouldBe60ForTractorWithMax()
        {
            var machine = new Machine() { type = 2 };

            Assert.Equal(60, machine.getMaxSpeed(machine.type));
        }

        /// <summary>
        /// Tests for Refactored Class
        /// </summary>

        [Fact]
        public void NameShouldBeTractor_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Tractor);

            Assert.Equal("tractor", machine.Name);
        }

        [Fact]
        public void DescriptionShouldBeGreenTractor90_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Tractor);

            Assert.Equal(" green tractor [90].", machine.Description);
        }

        [Fact]
        public void ColorShouldBeGreenForTractor_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Tractor);

            Assert.Equal(BaseColors.Green, machine.BaseColor);
        }

        [Fact]
        public void TrimColorShouldBeGoldForTractor_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Tractor);

            Assert.Equal(TrimColors.Gold, machine.TrimColor);
        }

        [Fact]
        public void MaxSpeedShouldBe90ForTractorWithNoMax_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Tractor);

            Assert.Equal(90, machine.GetMaxSpeed(true));
        }


        [Fact]
        public void MaxSpeedShouldBe60ForTractorWithMax_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Tractor);

            Assert.Equal(60, machine.GetMaxSpeed());
        }
    }
}

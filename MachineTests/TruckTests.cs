﻿using FortCodeExercises.Exercise1;
using Xunit;


namespace FortCodeTests
{
    public class TruckTests
    {
        /// <summary>
        /// Tests for Original Class
        /// </summary>

        [Fact]
        public void NameShouldBeTruck()
        {
            var machine = new Machine() { type = 3 };

            Assert.Equal("truck", machine.name);
        }

        [Fact]
        public void DescriptionShouldBeYellowTruck70()
        {
            var machine = new Machine() { type = 3 };

            Assert.Equal(" yellow truck [70].", machine.description);
        }

        [Fact]
        public void ColorShouldBeYellowForTruck()
        {
            var machine = new Machine() { type = 3 };

            Assert.Equal("yellow", machine.color);
        }

        [Fact]
        public void TrimColorShouldBeEmptyForTruck()
        {
            var machine = new Machine() { type = 3 };

            Assert.Equal(string.Empty, machine.trimColor);
        }

        [Fact]
        public void MaxSpeedShouldBe70ForTruckWithNoMax()
        {
            var machine = new Machine() { type = 3 };

            Assert.Equal(70, machine.getMaxSpeed(machine.type, true));
        }


        [Fact]
        public void MaxSpeedShouldBe70ForTruckWithMax()
        {
            var machine = new Machine() { type = 3 };

            Assert.Equal(70, machine.getMaxSpeed(machine.type));
        }

        /// <summary>
        /// Tests for Refactored Class
        /// </summary>

        [Fact]
        public void NameShouldBeTruck_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Truck);

            Assert.Equal("truck", machine.Name);
        }

        [Fact]
        public void DescriptionShouldBeYellowTruck70_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Truck);

            Assert.Equal(" yellow truck [70].", machine.Description);
        }

        [Fact]
        public void ColorShouldBeYellowForTruck_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Truck);

            Assert.Equal(BaseColors.Yellow, machine.BaseColor);
        }

        [Fact]
        public void TrimColorShouldBeEmptyForTruck_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Truck);

            Assert.Equal(TrimColors.None, machine.TrimColor);
        }

        [Fact]
        public void MaxSpeedShouldBe70ForTruckWithNoMax_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Truck);

            Assert.Equal(70, machine.GetMaxSpeed(true));
        }


        [Fact]
        public void MaxSpeedShouldBe70ForTruckWithMax_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Truck);

            Assert.Equal(70, machine.GetMaxSpeed());
        }
    }
}

﻿using FortCodeExercises.Exercise1;
using Xunit;


namespace FortCodeTests
{
    public class CraneTests
    {
        /// <summary>
        /// Tests for Original Class
        /// </summary>

        [Fact]
        public void NameShouldBeCrane()
        {
            var machine = new Machine() { type = 1 };

            Assert.Equal("crane", machine.name);
        }

        [Fact]
        public void DescriptionShouldBeBlueCrane75()
        {
            var machine = new Machine() { type = 1 };

            Assert.Equal(" blue crane [75].", machine.description);
        }

        [Fact]
        public void ColorShouldBeBlueForCrane()
        {
            var machine = new Machine() { type = 1 };

            Assert.Equal("blue", machine.color);
        }

        [Fact]
        public void TrimColorShouldBeWhiteForCrane()
        {
            var machine = new Machine() { type = 1 };

            Assert.Equal("white", machine.trimColor);
        }

        [Fact]
        public void MaxSpeedShouldBe75ForCraneWithNoMax()
        {
            var machine = new Machine() { type = 1 };

            Assert.Equal(75, machine.getMaxSpeed(machine.type, true));
        }

        [Fact]
        public void MaxSpeedShouldBe70ForCraneWithMax()
        {
            var machine = new Machine() { type = 1 };

            Assert.Equal(70, machine.getMaxSpeed(machine.type));
        }

        /// <summary>
        /// Tests for Refactored Class
        /// </summary>

        [Fact]
        public void NameShouldBeCrane_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Crane);

            Assert.Equal("crane", machine.Name);
        }

        [Fact]
        public void DescriptionShouldBeBlueCrane75_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Crane);

            Assert.Equal(" blue crane [75].", machine.Description);
        }

        [Fact]
        public void ColorShouldBeBlueForCrane_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Crane);

            Assert.Equal(BaseColors.Blue, machine.BaseColor);
        }

        [Fact]
        public void TrimColorShouldBeWhiteForCrane_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Crane);

            Assert.Equal(TrimColors.White, machine.TrimColor);
        }

        [Fact]
        public void MaxSpeedShouldBe75ForCraneWithNoMax_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Crane);

            Assert.Equal(75, machine.GetMaxSpeed(true));
        }

        [Fact]
        public void MaxSpeedShouldBe70ForCraneWithMax_v2()
        {
            var machine = MachineFactory.Create(MachineTypes.Crane);

            Assert.Equal(70, machine.GetMaxSpeed());
        }
    }
}

﻿using FortCodeExercises.Exercise1;
using System;
using Xunit;

namespace FortCodeTests
{
    public class MachineFactoryTests
    {
        [Fact]
        public void InvalidMachineTypeShouldThrowArgumentException()
        {
            Assert.Throws<ArgumentException>( () => MachineFactory.Create(MachineTypes.NotValidMachineType));
        }
    }
}
